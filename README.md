# Env Prepare
1. Download from https://golang.google.cn/dl/
2. Config local env variables(in Windows go to Environment Variables to add variables and add GOROOT/bin to the Path)
```
export GOROOT=where you install the go from step 1
export PATH=$PATH:$GOROOT/bin
export GOPROXY=https://goproxy.cn,direct
export GOPATH=the go path folder
export GO111MODULE=on
```
3. Install the GO extension in vscode
4. in vscode set settings below:
```
    "gopls": {
        "build.experimentalWorkspaceModule": true
    }
```
5. In vscode: Setting => Command Palette => Go:Install/Update Tools => Select all to install, all the go tools will be installed in your gopath folder
6. Git Operations:
```
git init
git branch -m main
git add --all
git commit -m "xxx"
git remote add origin https://xxxx.git
git push -u origin main
```
# Go MOD steps:
1. mkdir xxx
2. cd xxx
3. go mod init bitbucket.org/zwloveu/go-tutorial
4. for one workspace: go mod edit -replace bitbucket.org/zwloveu/go-tutorial/greetings=./greetings
5. for separate workspace: go get -u bitbucket.org/zwloveu/go-tutorial/greetings
6. write code
7. go mod tidy