package main

import (
	"bufio"
	"fmt"
	"math"
	"os"

	"bitbucket.org/zwloveu/go-tutorial/pkg/examples"
	"github.com/google/go-cmp/cmp"
)

type Op func(x float64) float64

func Map(op Op, a []float64) []float64 {
	res := make([]float64, len(a))
	for i, x := range a {
		res[i] = op(x)
	}

	return res
}

type Point struct {
	X    float32
	Y    float32
	Z    float32
	Name []string
}

func (p1 Point) Equal(p2 Point) bool {
	if p1.X == p2.X && p1.Y == p2.Y && p1.Z == p2.Z {
		return true
	}
	return false
}

func main() {
	examples.RunSlices()

	op := math.Abs
	a := []float64{1, -2}
	b := Map(op, a)
	fmt.Println(b)

	c := Map(func(x float64) float64 { return 10 * x }, b)
	fmt.Println(c)

	p1 := Point{3, 2, 1, []string{"a"}}
	p2 := Point{3, 2, 1, []string{"b"}}
	fmt.Println(cmp.Equal(p1, p2))

	fmt.Print("Enter a grade:")
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	fmt.Println(input)
}
