// Slices are a key data type in GO,
// giving a more powerful interface to sequences than arrays
// Unlike arrays, slices are typed only by the elements then contain
// To create an empty slice with no-zero length, use the builtin make

package examples

import (
	"fmt"
	"sort"
)

type kv struct {
	key   string
	value int
}

type kvPairs []kv

// Implement Len() int, Less(i,j int) bool, Swap(i,j int) to Sort
func (p kvPairs) Len() int {
	return len(p)
}

func (p kvPairs) Less(i, j int) bool {
	return p[i].value < p[j].value
}

func (p kvPairs) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func RunSlices() {
	// Here we make a slice of strings of length 3(initially zero-valued)
	s := make([]string, 3)
	fmt.Println("empty:", s)

	s[0] = "a"
	s[1] = "b"
	s[2] = "c"
	fmt.Println("set", s)
	fmt.Println("get", s[2])

	// Slices support several more that make them richer than arrays.
	// One is the builtin append, which returns a slice containing
	// one ore more new values
	// Note that we need to accept a return value
	// from append as we may get a new slice value
	s = append(s, "d")
	s = append(s, "e", "f")
	fmt.Println("append", s)

	// Slices can also be copy'd.
	// Here we create an empty slice c of the
	// same length as s and copy into c from s
	c := make([]string, len(s))
	copy(c, s)
	fmt.Println("cpy", c)

	// Slices support a "slice" operator with the syntax
	// slice[low:high].
	// Here we get a slice of the elements s[2],s[3] and s[4]
	l := s[2:5]
	fmt.Println("sl1", l)

	// Here slices up(but excluding) s[5]: s[0]-s[4]
	l = s[:5]
	fmt.Println("sl2", l)

	// Here slices up from(add including) s[2]: s[2]-s[len(s)-1]
	l = s[2:]
	fmt.Println("sl3", l)

	// We can declare and initialize a variable for slice
	// in a single line as well
	t := []string{"g", "h", "i"}
	fmt.Println("dcl:", t)

	// Slices can be composed into multi-dimensional data structures.
	// The length of the inner slices can vary,
	// unlike with multi-dimensional arrays
	twoD := make([][]int, 3)
	for i := 0; i < 3; i++ {
		innerLen := i + 1
		twoD[i] = make([]int, innerLen)
		for j := 0; j < innerLen; j++ {
			twoD[i][j] = i + j
		}
	}
	fmt.Println("2d:", twoD)

	sortSlice()
}

func sortSlice() {
	heights := map[string]int{
		"Peter": 170,
		"John":  175,
		"Tony":  180,
	}

	// heights := make(map[string]int)
	// heights["Peter"] = 170
	// heights["Joan"] = 168
	// heights["Jan"] = 175

	p := make(kvPairs, len(heights))
	i := 0
	for k, v := range heights {
		p[i] = kv{k, v}
		i++
	}

	sort.Sort(p)
	fmt.Println(p)
	for _, v := range p {
		fmt.Println(v)
	}
}
